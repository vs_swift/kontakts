//
//  AdressBookTableViewController.swift
//  kontakts
//
//  Created by Private on 1/29/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit

class AdressBookTableViewController: UITableViewController {

    var namesDictionary = [String: [String]]()
    var nameSectionTitles = [String]()
    var names = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        names = ["Sondra Falgout","Jeanine Szabo","Ardis Gorder","Eleonore Ariola","Jazmine Monson","Lashunda Winebarger","Bong Geoghegan","Nohemi Finkle","Ceola Nickel","Lynwood Vanvalkenburg","Angelic Karlin","Otha Tetzlaff","Danica Lepley","Kari Hereford","Nada Sano","Linn Decuir","Lacie Varn","Ariel David","Rima Thurgood","Rose Shehane","Agnes Rolan","Carlton Scriven","Burl Dykes","Carmen Couturier","Allyson Schumaker","Fredericka Gamon","Kenya Willaims","Junita Lipe","Ludivina Propst","Phebe Ohare","Aldo Winebrenner","Luise Langlinais","Moriah Trader","Buena Beckwith","Ramon Ikeda","Horace Lemieux","Joie Alling","Terrence Whitmarsh","Salvatore Stuber","Katie Filler","Kellie Painter","Laila Seese","Teresia Proctor","Dalia Mcgowen","Jonathan Denley","Twila Schaffner","Shawna Reel","Yvonne Mcgruder","Darcie Wiesner","Scarlett Rieger"]
        
        for name in names {
            let nameKey = String(name.prefix(1))
            if var nameValues = namesDictionary[nameKey] {
                nameValues.append(name)
                namesDictionary[nameKey] = nameValues
            } else {
                namesDictionary[nameKey] = [name]
            }
        }
        
        nameSectionTitles = [String](namesDictionary.keys)
        nameSectionTitles = nameSectionTitles.sorted(by: { $0 < $1 })
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return nameSectionTitles.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let nameKey = nameSectionTitles[section]
        if let nameValues = namesDictionary[nameKey] {
            return nameValues.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let nameKey = nameSectionTitles[indexPath.section]
        if let nameValues = namesDictionary[nameKey] {
            cell.textLabel?.text = nameValues[indexPath.row]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nameSectionTitles[section]
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return nameSectionTitles
    }
}

 